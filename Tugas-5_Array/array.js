//Soal No. 1 (Range)
console.log("Soal No. 1 (Range)");
function range(startNum, finishNum) {
  const arrNum = [];
  if (!startNum || !finishNum) {
    return -1;
  }
  if (startNum < finishNum) {
    for (let i = startNum; i <= finishNum; i++) {
      arrNum.push(i);
    }
  }
  if (startNum > finishNum) {
    for (let i = startNum; i >= finishNum; i--) {
      arrNum.push(i);
    }
  }
  return arrNum;
}

console.log(range(1, 10)); //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)); // -1
console.log(range(11, 18)); // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)); // [54, 53, 52, 51, 50]
console.log(range()); // -1
console.log("\n\n");

//Soal No. 2 (Range with Step)
console.log("Soal No. 2 (Range with Step)");
function rangeWithStep(startNum, finishNum, step) {
  const arrNum = [];
  if (!startNum || !finishNum) {
    return -1;
  }
  if (startNum < finishNum) {
    for (let i = startNum; i <= finishNum; i += step) {
      arrNum.push(i);
    }
  }
  if (startNum > finishNum) {
    for (let i = startNum; i >= finishNum; i -= step) {
      arrNum.push(i);
    }
  }
  return arrNum;
}

console.log(rangeWithStep(1, 10, 2)); // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)); // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)); // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)); // [29, 25, 21, 17, 13, 9, 5]
console.log("\n\n");

//Soal No. 3 (Sum of Range)
console.log("Soal No. 3 (Sum of Range)");
function sum(a, b, c) {
  if (!c) {
    c = 1;
  }
  if (!b) {
    return !a ? (a = 0) : a;
  } else {
    return rangeWithStep(a, b, c).reduce((x, y) => x + y);
  }
}

console.log(sum(1, 10)); // 55
console.log(sum(5, 50, 2)); // 621
console.log(sum(15, 10)); // 75
console.log(sum(20, 10, 2)); // 90
console.log(sum(1)); // 1
console.log(sum()); // 0
console.log("\n\n");

//Soal No. 4 (Array Multidimensi)
console.log("Soal No. 4 (Array Multidimensi)");
var input = [
  ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
  ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
  ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
  ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"],
];

function dataHandling(arr) {
  for (let i = 0; i < arr.length; i++) {
    console.log(
      "Nomor ID: " +
        arr[i][0] +
        "\nNama Lengkap: " +
        arr[i][1] +
        "\nTTL: " +
        arr[i][2] +
        " " +
        arr[i][3] +
        "\nHobi: " +
        arr[i][4] +
        "\n===================="
    );
  }
}

dataHandling(input);
console.log("\n\n");

//Soal No. 5 (Balik Kata)
console.log("Soal No. 5 (Balik Kata)");

function balikKata(kalimat) {
  let dibalik = "";
  for (let i = 1; i <= kalimat.length; i++) {
    kalimat.length - i == -1
      ? (dibalik += kalimat[0])
      : (dibalik += kalimat[kalimat.length - i]);
  }
  return dibalik;
  // return kalimat[0 - 1];
}

console.log(balikKata("Kasur Rusak")); // kasuR rusaK
console.log(balikKata("SanberCode")); // edoCrebnaS
console.log(balikKata("Haji Ijah")); // hajI ijaH
console.log(balikKata("racecar")); // racecar
console.log(balikKata("I am Sanbers")); // srebnaS ma I
console.log("\n\n");

//Soal No. 6 (Metode Array)
console.log("Soal No. 6 (Metode Array)");
let inpt = [
  "0001",
  "Roman Alamsyah",
  "Bandar Lampung",
  "21/05/1989",
  "Membaca",
];

function dataHandling2(params) {
  const newData = params;
  newData.splice(1, 1, newData[1] + " Elsharawy");
  newData.splice(2, 1, "Provinsi " + newData[2]);
  newData.splice(4, 0, "Pria");
  newData.splice(5, 1, "SMA Internasional Metro");
  // console.log(newData);
  console.log(newData);
  const tanggal = newData[3].split("/");
  switch (+tanggal[1]) {
    case 01:
      console.log("Januari");
      break;
    case 02:
      console.log("Februari");
      break;
    case 03:
      console.log("Maret");
      break;
    case 04:
      console.log("April");
      break;
    case 05:
      console.log("Mei");
      break;
    case 06:
      console.log("Juni");
      break;
    case 07:
      console.log("Juli");
      break;
    case 08:
      console.log("Agustus");
      break;
    case 09:
      console.log("September");
      break;
    case 10:
      console.log("Oktober");
      break;
    case 11:
      console.log("November");
      break;
    case 12:
      console.log("Desember");
      break;
    default:
      console.log(tanggal[1]);
      break;
  }
  console.log(tanggal.sort((a, b) => b - a));
  console.log(newData[3].split("/").join("-"));
  console.log(newData[1].slice(0, 14));
}
dataHandling2(inpt);

/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */
