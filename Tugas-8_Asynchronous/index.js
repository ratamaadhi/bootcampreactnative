const readBooks = require("./callback.js");

let books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
];

// Tulis code untuk memanggil function readBooks di sini
// console.log(typeof readBooks);

function baca(waktu, buku, index = 0) {
  if (buku.length > index) {
    readBooks(waktu, buku[index], (sisa) => {
      if (waktu > sisa) {
        baca(sisa, buku, (index += 1));
      }
    });
  } else {
    console.log(`list buku habis`);
  }
}
baca(9000, books, 0);
