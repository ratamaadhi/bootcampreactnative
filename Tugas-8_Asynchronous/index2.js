const readBooksPromise = require("./promise.js");

const books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
];

// Lanjutkan code untuk menjalankan function readBooksPromise
function panggilBook(time, book, i = 0) {
  readBooksPromise(time, book[i])
    .then((res) => {
      // console.log("res =>", res);
      i += 1;
      panggilBook(res, book, i);
    })
    .catch((err) => {
      return err;
      // console.log(err);
    });
}

// readBooksPromise(10000, books[0]);
panggilBook(10000, books, 0);
