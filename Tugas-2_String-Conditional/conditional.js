var nama = "Adhi";
var peran = "Batu";

//conditional if
if (nama === "" && peran === "") {
  console.log("Nama harus diisi!");
} else if (nama !== "" && peran === "") {
  console.log("Halo " + nama + ", Pilih peranmu untuk memulai game!");
} else if (nama !== "" && peran === "penyihir") {
  console.log("Selamat datang di Dunia Werewolf, " + nama);
  console.log(
    "Halo " +
      peran +
      nama +
      " , kamu dapat melihat siapa yang menjadi werewolf!"
  );
} else if (nama !== "" && peran === "guard") {
  console.log("Selamat datang di Dunia Werewolf, " + nama);
  console.log(
    "Halo " +
      peran +
      nama +
      " , kamu akan membantu melindungi temanmu dari serangan werewolf."
  );
} else if (nama !== "" && peran === "warewolf") {
  console.log("Selamat datang di Dunia Werewolf, " + nama);
  console.log(
    "Halo " + peran + nama + ", Kamu akan memakan mangsa setiap malam!"
  );
} else {
  console.log("halo " + nama + ", ketikan peranmu dengan benar.");
}

// switch case
var tanggal = 1; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan = 13; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun = 1900; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)

switch (bulan) {
  case 1:
    console.log(tanggal + " Januari " + tahun);
    break;
  case 2:
    console.log(tanggal + " Februari " + tahun);
    break;
  case 3:
    console.log(tanggal + " Maret " + tahun);
    break;
  case 4:
    console.log(tanggal + " April " + tahun);
    break;
  case 5:
    console.log(tanggal + " Mei " + tahun);
    break;
  case 6:
    console.log(tanggal + " Juni " + tahun);
    break;
  case 7:
    console.log(tanggal + " Juli " + tahun);
    break;
  case 8:
    console.log(tanggal + " Agustus " + tahun);
    break;
  case 9:
    console.log(tanggal + " September " + tahun);
    break;
  case 10:
    console.log(tanggal + " Oktober " + tahun);
    break;
  case 11:
    console.log(tanggal + " November " + tahun);
    break;
  case 12:
    console.log(tanggal + " Desember " + tahun);
    break;
  default:
    console.log("isikan bulan dari 1 - 12");
    break;
}
